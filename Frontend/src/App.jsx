import { useState, useEffect } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0)
  const [message, setMessage] = useState('');
  const [num1, setNum1] = useState('');
  const [num2, setNum2] = useState('');
  const [result, setResult] = useState('');


  useEffect(() => {
    fetch('http://localhost:3000/message')
      .then(response => response.text())
      .then(data => setMessage(data))
      .catch(error => console.error('Error al obtener el mensaje:', error));
  }, []); 

 
  const multiply = async () => {
    try {
      const response = await fetch('http://localhost:3000/multiply', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ num1: Number(num1), num2: Number(num2) })
      });
      const data = await response.json();
      setResult(data.result);
    } catch (error) {
      console.error('Error al multiplicar:', error);
    }
  };


  return (
    <div className="container mt-5">
      <h1>{message}</h1>
      <div className="card">
        <div className="card-body">
          <div className="mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Número 1"
              value={num1}
              onChange={(e) => setNum1(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Número 2"
              value={num2}
              onChange={(e) => setNum2(e.target.value)}
            />
          </div>
          <button className="btn btn-primary mb-3" onClick={multiply}>
            Multiplicar
          </button>
          {result !== null && <p>El resultado es: {result}</p>}
        </div>
      </div>      
    </div>
  );
}

export default App
